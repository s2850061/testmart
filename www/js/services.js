martialManager.factory('studentService', function($firebaseArray) {
    var fb = new Firebase("https://test-fire1010.firebaseio.com");
    var studs = $firebaseArray(fb);
    var studentService = {
        all: studs,
        get: function(studId) {
            return studs.$getRecord(studId);
        }
    };
    return studentService;
});