// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var martialManager = angular.module('martialManager', ['ionic', 'firebase'])
var fb = null;

martialManager.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
    var fb = new Firebase("https://test-fire1010.firebaseio.com");
  });
});

martialManager.config(function($stateProvider, $urlRouterProvider) {
  
  $stateProvider.state("home", {
    url: "/",
    templateUrl: "home.html"
  });
  
  $stateProvider.state("login", {
    url: "/login",
    templateUrl: "login.html",
    controller: 'LoginController'
  });
  
  
  $stateProvider.state("studList", {
    url: "/studList",
    templateUrl: "studList.html",
    controller: "listController"
  });
  
  $stateProvider.state("singleStudent", {
    url: "/:id",
    templateUrl: "singleStud.html",
    controler: "studentController"
  });
  
  $stateProvider.state("add", {
    url: "/add:",
    templateUrl: "add.html",
    controler: "addController"
  });
  
  $stateProvider.state("del", {
    url: "/del",
    templateUrl: "delStud.html",
    controler: "deleteController"
  });
  
  $stateProvider.state("edit", {
    url: "/edit",
    templateUrl: "edit.html",
    controler: "editController"
  });
  
  $stateProvider.state("one", {
    url: "/edit/:id",
    templateUrl: "editOne.html",
    controler: "studentEditController"
  });
  
  $urlRouterProvider.otherwise("/login");
  
});


