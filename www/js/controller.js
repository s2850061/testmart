var fb = new Firebase("https://test-fire1010.firebaseio.com");

//add controller
martialManager.controller('addController', function($scope,$firebaseArray,$firebaseObject,$state,studentService){
  
  $scope.submitStudent = function(){
    $scope.newStud = studentService.all;
    $scope.newStud.$add({
        studentFirstName: $scope.studFirstName,
        studentLastName: $scope.studLastName,
        studentEmail: $scope.studEmail
    });
    $state.go('home');
  };
});

martialManager.controller('listController', function($scope, studentService){
    $scope.students = studentService.all;
});

martialManager.controller('studentController', function($scope,studentService,$stateParams,$state){
    $scope.singleStudent = studentService.get($stateParams.id);
});

martialManager.controller("LoginController", function($scope, $firebaseAuth, $location) {
 
    $scope.login = function(username, password) {
        var fbAuth = $firebaseAuth(fb);
        fbAuth.$authWithPassword({
            email: username,
            password: password
        }).then(function(authData) {
            $location.path("/");
        }).catch(function(error) {
            alert(error);
        });
    }
    
    $scope.logout = function() {
      var fbAuth = $firebaseAuth(fb);
        fb.unauth();
        alert("You are now logged out");
        $location.path("/login");
    };
 
    $scope.register = function(username, password) {
        var fbAuth = $firebaseAuth(fb);
        fbAuth.$createUser({email: username, password: password}).then(function() {
            return fbAuth.$authWithPassword({
                email: username,
                password: password
            });
        }).then(function(authData) {
            $location.path("/");
            alert("Thank you for registering");
        }).catch(function(error) {
            alert(error);
        });
    }
});